function [ A, misc ] = create_dynamics( xDim, dyn_type)
%CREATE_DYNAMICS Summary of this function goes here
%   Detailed explanation goes here

yDim = xDim;
p = LDSgenerateExample('xDim',xDim,'yDim',yDim,'doff',-2.0);
switch dyn_type
    case 'real_stable'
        d = abs(randn(xDim,1));
        d = sort(d/(1.3*max(d)),'descend');
        v = orth(randn(xDim));
        u = orth(randn(xDim));
        v = [v(:,1) u*v(:,2)];

    case 'imag_stable'
        a = 0:.001:.999; a = a(randperm(numel(a),1));
        b = randn; c = randn; f = randn; e = randn;
        d = [a + b*1i; a - b*1i];
        v = [c c ; f + e*1i f - e*1i];
        
    case 'imag_specific1'
        a = .95;
        b = .15; c = randn; f = randn; e = randn;
        d = [a + b*1i; a - b*1i];
        v = [c c ; f + e*1i f - e*1i];
        
    case 'given'
        [v, d] = eig(p.model.A);
        d = diag(d);
end
misc = p;
A = real(v*diag(d)/v);

end

