[~,~,~,~,compEnv,hm] = set_local_path('lds');
if strcmp(compEnv,'pace')
    r_dir = 'Versioned';
else
    r_dir = 'Research';
end
working_dir = fullfile(hm,r_dir,'lds','examples');
cd(working_dir)
addpath(genpath(fullfile(hm,r_dir,'parampack')));
addpath(genpath(fullfile(hm,r_dir,'Packages','export_fig')));
proj_path = fullfile(hm,r_dir,'lds');
addpath(genpath(proj_path));