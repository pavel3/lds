clc
clear

addpath(genpath('/home/pav/Research/lds/'));

xDim = 2;
yDim = 2;
Trials = 25;

sysNum = 3;

B = zeros(xDim, xDim, sysNum);

t = 0:.1:5;

c = zeros(sysNum, numel(t));

for ii = 1:sysNum-2
    temp = LDSgenerateExample('xDim',xDim,'yDim',yDim,'doff',-2.0);
    B(:,:,ii) = temp.model.A;
end
B(:,:,end-1) = create_dynamics(xDim,'imag_stable');
B(:,:,end) = create_dynamics(xDim,'real_stable');
B = real(B);

por = [.1 .1 .7];

cp = randperm(round(numel(t)*por(1)),1) + round(numel(t)*por(2));
cp_end = cp + round(numel(t)*por(3));

for ii = 1:sysNum
    v1 = rand; v2 = rand;
    c(ii,1:cp) = v1;
    c(ii,cp+1:cp_end) = linspace(v1,v2,cp_end-cp);
    c(ii,cp_end+1:end) = v2;
end

c = c./sum(c,1);

A = zeros(xDim,xDim,numel(t));
eigA = zeros(xDim,numel(t));
for ii = 1:numel(t)
    for jj = 1:sysNum
        A(:,:,ii) = A(:,:,ii) + c(jj,ii)*B(:,:,jj);
    end
    eigA(:,ii) = eig(A(:,:,ii));
end

%figure;
clf
subplot(2,3,[1 4])
plot(c')
title('coefficients')
xlabel('time')

subplot(2,3,[2 5])
hold on
for ii = 1:Trials
    x = zeros(xDim,numel(t));
    x(:,1) = randn(xDim,1);
    for jj = 2:numel(t)
        x(:,jj) = A(:,:,jj)*x(:,jj-1);
    end
    plot(x(1,:),x(2,:))
end
hold off
title('trajectories over time')
xlabel('x1')
ylabel('x2')

% subplot(2,3,[3 6])
% hold on
% scatter(real(eigA(1,:)),imag(eigA(1,:)),[],1:numel(t),'x','LineWidth',2)
% scatter(real(eigA(2,:)),imag(eigA(2,:)),[],1:numel(t),'s','LineWidth',2)
% hold off

subplot(2,3,3)
plot(real(eigA)')
title('real(eig) of A')
xlabel('time')
ylim([min(0,min(real(eigA(:)))*1.2) max(1,max(real(eigA(:)))*1.2)])

subplot(2,3,6)
plot(imag(eigA)')
title('imag(eig) of A')
xlabel('time')
ylim([min(0,min(imag(eigA(:)))*1.2) max(imag(eigA(:)))*1.2])


