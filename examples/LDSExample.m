% PLDS toolbox example
% 
%
%
% Lars Buesing, Jakob H Macke, 2014
%

paths_set_ex

clear
close all


% set parameters for the ground truth model
 
xDim    = 2;												% latent dimensiom
yDim    = 20;											    	% observed dimension = no of neurons
T       = 100;												% no of time bins per trial
Trials  = 20;		    										% no trials



%%%% ground truth model

trueparams = LDSgenerateExample('xDim',xDim,'yDim',yDim,'doff',-2.0);                                   % create ground truth model parameters
seqOrig    = LDSsample(trueparams,T,Trials);								% sample from the model


params = trueparams;
seqP    = seqOrig;

%p start

naive_data_gen
seqOrig = seqP;
seq = seqP;
trueparams.model.A = A;

% p end

% Inference
seq = LDSInference(params,seq);

% Mstep for parameters A,Q,Q0,0

params = LDSMStepLDS(params,seq);

%%
figure;
subplot(1,3,1)
hold on
for tr = 1:Trials
    x = seqOrig(tr).x;
    plot(x(1,:),x(2,:))
end
title('original trajectories')
axis square
hold off
xl = get(gca,'XLim'); yl = get(gca,'YLim');

subplot(1,3,2)
hold on
for tr = 1:Trials
    x = seq(tr).posterior.xsm;
    plot(x(1,:),x(2,:))
end
title('recovered trajectories')
xlim([xl]); ylim([yl]);
axis square
hold off

subplot(1,3,3)
hold on
for tr = 1:Trials
    x = seq(tr).posterior.xsm;
    scatter(x(1,:),x(2,:),[],[1:size(x,2)])
end
title('recovered trajectories over time')
axis square
hold off

figure;
for tr = 1:Trials
    x = seqOrig(tr).x;
    subplot(2,2,1)
    hold on
    plot(x(1,:))
    hold off
    subplot(2,2,2)
    hold on
    plot(x(2,:))
    hold off
end
subplot(2,2,1)
title('original trajectories, 1')
subplot(2,2,2)
title('original trajectories, 2')

for tr = 1:Trials
    x = seq(tr).posterior.xsm;
    subplot(2,2,3)
    hold on
    plot(x(1,:))
    hold off
    subplot(2,2,4)
    hold on
    plot(x(2,:))
    hold off
end
subplot(2,2,3)
title('recovered trajectories, 1')
subplot(2,2,4)
title('recovered trajectories, 2')

% x0vec = zeros(2,Trials);
% for ii = 1:Trials
%     x0vec(:,ii) = seq(ii).posterior.xsm(:,1);
% end

%%
trueparams.model.A
params.model.A

[ta, tb] = eig(trueparams.model.A)
[ra, rb] = eig(params.model.A)

%%
% figure(10)
% for ii = 1:Trials 
%     clf
%     
%     subplot(2,1,1)
%     hold on
%     plot(seq(ii).x(1,:)','Color','b')
%     plot(seq(ii).posterior.xsm(1,:)','Color','r')
%     hold off
%     subplot(2,1,2)
%     hold on
%     plot(seq(ii).x(2,:)','Color','b')
%     plot(seq(ii).posterior.xsm(2,:)','Color','r')
%     hold off
%     drawnow
%     pause
% end

xv = zeros(2,T,Trials);
xsmv = zeros(2,T,Trials);
for ii = 1:Trials
    xv(:,:,ii) = seq(ii).x;
    xsmv(:,:,ii) = seq(ii).posterior.xsm;
end
xv1 = xv(1,:,:); xv1 = xv1(:);
xv2 = xv(2,:,:); xv2 = xv2(:);
xsmv1 = xsmv(1,:,:); xsmv1 = xsmv1(:);
xsmv2 = xsmv(2,:,:); xsmv2 = xsmv2(:);

figure(11)
clf
subplot(1,2,1)
hold on
scatter(xv1,xsmv1)
plot(-3:.01:3,-3:.01:3,'-k')
hold off
axis square
subplot(1,2,2)
hold on
plot(-2:.01:2,-2:.01:2,'-k')
scatter(xv2,xsmv2)
hold off
axis square
