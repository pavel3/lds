% seqOrig must have x, y, T, and yr
%%
dyn_type = 'imag_specific1';
[A, misc] = create_dynamics(xDim, dyn_type);
C  = randn(yDim,xDim)./(3*xDim);
%C = eye(yDim,xDim);
Q = diag(rand(xDim,1))/10;
x0 = randn(xDim,1);
mu = zeros(xDim,1);
x = zeros(xDim,T,Trials);
d  = 0.3*randn(yDim,1);
params = misc;
params.model.A = A;
params.model.Q = Q;
params.model.Q0 = Q;
params.model.x0 = x0;
params.model.C = C;
params.model.d = d;
params.model.R = diag(rand(yDim,1));
params.model.Pi = 0;

%figure;
%hold on
for tr = 1:Trials
    x(:,1,tr) = x0;
    for t = 2:T
        x(:,t,tr) = A*x(:,t-1,tr) + mvnrnd(mu,Q)';
    end
    %plot(x(1,:,tr),x(2,:,tr))
    seqOri(tr).x = x(:,:,tr);
    seqOri(tr).y = C*x(:,:,tr) + repmat(d,1,T);
    seqOri(tr).T = T;
end
%hold off
      



%%
% u = orth(randn(xDim)); s = abs(randn(xDim,1));
% s = sort(s/(1.1*max(s)),'descend');
% %A = u*diag(s)*/u;
% %C  = randn(yDim,xDim)./(3*xDim);
% mu = zeros(xDim,1);
% Q = randn(xDim); Q = Q'*Q; Q = Q/(10*max(abs(eig(Q))));
% Q = zeros(xDim);
% d = zeros(yDim,1);
% 
% x = zeros(xDim, T);
% x0 = randn(xDim,1);
% yr = zeros(yDim,T);
% for tr = 1:Trials
%     for tt = 1:T
%         if tt == 1
%             x(:,tt) = x0;
%         else
%             x(:,tt) = A*x(:,tt-1) + mvnrnd(mu,Q,1)';
%         end
%         yr(:,tt) = C*x(:,tt) + d;
%     end
%     seqP(tr).x = x;
%     seqP(tr).T = T;
%     seqP(tr).y = yr;
% end
