addpath(genpath('~/Research/Packages/Misc'))

%% reconstruct yr
for ii = 1:Trials
    seq(ii).yr = params.model.C*seq(ii).posterior.xsm;
end

%%

figure;
subplot(1,3,1)
hold on
for tr = 1:Trials
    x = seqOrig(tr).x;
    plot(x(1,:),x(2,:))
end
title('original trajectories')
axis square
hold off
xl = get(gca,'XLim'); yl = get(gca,'YLim');

subplot(1,3,2)
hold on
for tr = 1:Trials
    x = seq(tr).posterior.xsm;
    plot(x(1,:),x(2,:))
end
title('recovered trajectories')
xlim([xl]); ylim([yl]);
axis square
hold off

subplot(1,3,3)
hold on
for tr = 1:Trials
    x = seq(tr).posterior.xsm;
    scatter(x(1,:),x(2,:),[],[1:size(x,2)])
end
title('recovered trajectories over time')
axis square
hold off

figure;
for tr = 1:Trials
    x = seqOrig(tr).x;
    subplot(2,2,1)
    hold on
    plot(x(1,:))
    hold off
    subplot(2,2,2)
    hold on
    plot(x(2,:))
    hold off
end
subplot(2,2,1)
title('original trajectories, 1')
subplot(2,2,2)
title('original trajectories, 2')

for tr = 1:Trials
    x = seq(tr).posterior.xsm;
    subplot(2,2,3)
    hold on
    plot(x(1,:))
    hold off
    subplot(2,2,4)
    hold on
    plot(x(2,:))
    hold off
end
subplot(2,2,3)
title('recovered trajectories, 1')
subplot(2,2,4)
title('recovered trajectories, 2')

[ta, tb] = eig(tp.model.A)
[ra, rb] = eig(params.model.A)

%%
figure;
rnum = 3; cnum = 3;
yl = zeros(2,Trials);
for ii = 1:Trials
    yl(1,ii) = min(min(seqOrig(ii).yr(:)),min(seq(ii).yr(:)));
    yl(2,ii) = max(max(seqOrig(ii).yr(:)),max(seq(ii).yr(:)));
end
for ii = 1:rnum*cnum
    subplot(rnum,cnum,ii)
    plot(seqOrig(ii).yr')
    ylim(yl(:,ii))
    title(sprintf('yr tr = %d',ii))
end
figure;
for ii = 1:rnum*cnum
    subplot(rnum,cnum,ii)
    plot(seq(ii).yr')
    ylim(yl(:,ii))
    title(sprintf('yr rec tr = %d',ii))
end
%%
figure;
err_tr = zeros(Trials,T);
err_indx = 1:Trials; err_indx(13) = [];
for ii = err_indx
    err_tr(ii,:) = mnorm(seqOrig(ii).yr - seq(ii).yr,2)./mnorm(seqOrig(ii).yr,2);
end
plot(1:T,err_tr')
title('errors across time and trials')